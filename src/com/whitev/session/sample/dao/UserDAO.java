package com.whitev.session.sample.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import com.whitev.session.sample.db.tables.User;

public class UserDAO {
	/**
	 * 連線資料庫
	 * 由於無論是新增資料、取得資料還是其他，每次都要執行同樣 code ，所以獨立成一個 method
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	private SQLConnector connectDatabase() throws ClassNotFoundException, SQLException {
		 Class.forName("com.mysql.jdbc.Driver");
		 
		 Connection c = DriverManager.getConnection("jdbc:mysql://127.0.0.1/", "root", "1234");
         Statement s = c.createStatement();
         
         return new SQLConnector(c, s);
	}
	
	public User add(String account, String password, String name) throws ClassNotFoundException, SQLException {
		SQLConnector s = connectDatabase();
		
		// 使用 PreparedStatement 來處理 SQL 語句
		// 之所以使用 PreparedStatement 是因為 PreparedStatement 可以以「參數」的方式處理變數（見下面 p.setString）
		//
		// 另外，這段 SQL 中是假設資料庫表格的名稱為「user」
		PreparedStatement p = s.getConnection().prepareStatement("INSERT INTO user (	"
				+ "		account, password, name																"
				+ ") VALUES (																						"
				+ "		?, ?, ?																						"
				+ ")");
		
		// 處理參數（第一個 ? 要塞帳號的值，所以是 p.setString(1, account); ……依此類推）
		p.setString(1, account);
		p.setString(2, password);
		p.setString(3, name);
		
		// 處理好變數後，提交變更至資料庫
		p.executeUpdate();
		p.clearParameters();
		p.close();
		
		// 取得剛才新增的 user 的 ID
		int index = s.getStatement().executeQuery("SELECT LAST_INSERT_ID()").getInt(1);
		
		User u = new User(account, password, name);
		u.setId(index);
		
		// 資料庫都處理完後，關閉資料庫連線
		// 這步很重要！
		// 如果不這樣做，就會導致資料庫一直處在被連線狀態
		// 就像你 chrome 某個 tab 一直打開，即使你沒去使用該 tab ，但他會一直吃你的記憶體
		s.close();
		
		return u;
	}
	
}
