package com.whitev.session.sample.dao;
import java.sql.*;

/**
 * 連線 SQL 時紀錄 Connection 和 Statement 的 class
 * 
 * 藉由此 class 可以簡化程式碼
 */
public class SQLConnector {
	private Statement statement;
	private Connection connection;
	
	
	
	public Statement getStatement() {
		return statement;
	}

	public Connection getConnection() {
		return connection;
	}

	public SQLConnector(Connection connection, Statement statement) {
		super();
		this.statement = statement;
		this.connection = connection;
	}
	
	/**
	 * 關閉連線
	 * @throws SQLException
	 */
	public void close() throws SQLException {
		 if(!statement.isClosed()) {
			 statement.close();
         }
         if(!connection.isClosed()) {
        	 connection.close();
         }
	}
	
}
