<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%-- 
		下面這段是 jsp 特有的語法，表示在內部使用 java code
	--%>
	<%
		if("success".equals(request.getParameter("register"))){
			out.print("註冊成功");
		}else if("fail".equals(request.getParameter("register"))){
			out.print("註冊失敗");
		}
	%>
	
	<a href="${pageContext.request.contextPath}/">回到首頁</a>
</body>
</html>