<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8"/>
<title>註冊帳號</title>
</head>
<body>
	<form action="${pageContext.request.contextPath}/register/submit" autocomplete="off" method="post">
		<div>
			<label>
				帳號：
				<input name="account"/>
			</label>
		</div>
		<div>
			<label>
				密碼：
				<input name="password"/>
			</label>
		</div>
		<div>
			<label>
				使用者名稱：
				<input name="name"/>
			</label>
		</div>
		
		<button>送出</button>
	</form>
</body>
</html>