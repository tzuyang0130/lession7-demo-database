<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>首頁</title>
</head>
<body>
	<form action="#">
		
		<fieldset>
			<legend>登入</legend>
			<div>
				<label>
					帳號：
					<input name="account" disabled="disabled"/>
				</label>
			</div>
			<div>
				<label>
					密碼：
					<input name="password" disabled="disabled"/>
				</label>
			</div>
		</fieldset>
		
		<button disabled="disabled">登入</button>
	</form>
	<div style="font-family: serif; color: red;">這個範例僅有註冊帳號功能，無法登入</div>
	<div>
		<a href="${pageContext.request.contextPath}/register.jsp">註冊帳號</a>
	</div>
	
</body>
</html>